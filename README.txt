REQUIREMENTS
------------
 -No special requirements.

INSTALLATION
------------
 * Module: Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/extending-drupal/installing-modules
   for further information.

CONFIGURATION
-------------
Create new block to shown button for clean all facets filters.
